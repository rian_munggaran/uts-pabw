<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index()
    {
        return view('video', [
            'title' => 'All Posts',
            'posts' => Video::latest()->get()
        ]);
    }

    public function detail(Video $video)
    {
        return view('detail-video', [
            'title' => "Detail Video",
            'video' => $video
        ]);
    }

    public function datavideo()
    {
        return view('dashboard/video/data-video', [
            'title' => 'Data video',
            'video' => Video::latest()->get()
        ]);
    }

    public function tambahdatavideo()
    {
        return view('dashboard.video.tambah-data-video', [
            'title' => 'Tambah Data video',
            'video' => Video::latest()->get(),
            'kategori' => Kategori::all()
        ]);
    }

    public function tambahdata(Request $request)
    {
        $validatedData =  $request->validate([
            'nama' => 'required|max:255',
            'slug' => 'required|unique:artikels',
            'kategori_id' => 'required',
            'deskripsi' => 'required',
            'video-link' => 'required'
        ]);

        $validatedData['user_id'] = auth()->user()->id;

        Video::create($validatedData);

        return redirect('/dashboard/data-artikel')->with('success', 'Data berhasil ditambahkan!');
    }
}